﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ListShuffle
{
    class program
    {
        static void Main(string[] args)
        {
            List<string> sortedWords = new List<string>() { "Lala", "Onah", "Chidiebube", "Namzy", "Namzy-Lala" };
            var newRandom = new Random();
            var randomized = sortedWords.OrderBy(item => newRandom.Next());

            foreach (var value in randomized)
            {
                Console.WriteLine(value);
            }

            Console.WriteLine("\nQuery Syntax\n");

            var randomQuery = from word in sortedWords
                              orderby newRandom.Next()
                              select word;
            foreach (var randomWord in randomQuery)
            {
                Console.WriteLine(randomWord);
            }

        }
    }
}
