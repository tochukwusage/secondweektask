﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LoginSystem
{
    public class UserModel: EventArgs
    {
        public int Id { get; set; }
        public string BVN { get; set; }
        public string PIN { get; set; }
        public string Name { get; set;  }
    }
}
