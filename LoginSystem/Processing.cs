﻿using System;
using System.Collections.Generic;
using System.Text;
using LoginSystem;

namespace LoginSystem
{
    class Processing
    {
        private void ValidityCheck(string bvn, string pin)
        {
            Console.WriteLine($"This user: {pin} with BVN: {bvn} has been flagged");
            int n = 11;
            for (int i = 1; i < n; i++)
            {
                Console.Beep();
            }
        }
        public void FlagAccount(object person, UserModel model)
        {
            ValidityCheck(model.BVN, model.Name);
        }
    }
}
