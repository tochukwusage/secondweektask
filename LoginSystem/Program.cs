﻿using System;
using LoginSystem;
using System.Collections.Generic;
using System.Linq;
namespace LoginSystem
{
    class Program
    {
        static void Main(string[] args)
        {
            var atm = new ATM();
            var processing = new Processing();
            atm.Login += processing.FlagAccount;

            
            atm.UserLogin();

        }
    }
}
