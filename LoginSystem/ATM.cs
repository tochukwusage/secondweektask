﻿using System;
using System.Collections.Generic;
using System.Text;
using LoginSystem;
using System.Linq;
using System.Threading.Tasks;


namespace LoginSystem
{
    public class ATM
    {
        private static DataBase DataBase = new DataBase();
        public event EventHandler<UserModel> Login;
        public void UserLogin()
        {
            Console.WriteLine("Welcome to Ama banks. Please choose what you wanto do:" +
            "\n1. Create New Account\n2. Login with pin");

            Console.WriteLine("Please insert your card: ");
            var cardWithBVN = Console.ReadLine();

            Tuple<string, string, string> flaggedUsers;
            flaggedUsers = new Tuple<string, string, string>("012209", "012210", "012211");

            var queryDB = DataBase.Users()
                .Where(u => u.BVN.StartsWith(cardWithBVN))
                .First();

            while (queryDB == null)
            {
                cardWithBVN = ReenterDetail("BVN");
                break;

            }
            do
            {
                Console.WriteLine("Put your pin: ");
                var atmPin = Console.ReadLine();
                if (queryDB.PIN == atmPin)
                {
                    if (flaggedUsers.Item1 == queryDB.BVN || flaggedUsers.Item2 == queryDB.BVN || flaggedUsers.Item3 == queryDB.BVN)
                    {
                        OnLogin(new UserModel { BVN = queryDB.BVN, PIN = queryDB.PIN });
                    }
                    else
                    {
                        Console.WriteLine($"Welcome {queryDB.Name}");
                    }
                }
                else
                {
                    Console.WriteLine("Incorrect pin. Please try again");
                }
            } while (queryDB != null);
        }

        protected virtual void OnLogin(UserModel model)
        {
            Login?.Invoke(this, model);
        }
        private static string ReenterDetail(string fieldName)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine($"This {fieldName} was not found in our database.Do you want to register?");
            Console.ForegroundColor = ConsoleColor.White;
            return Console.ReadLine().Trim();
        }
    }
}
