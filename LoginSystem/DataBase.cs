﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;


namespace LoginSystem
{
    public class DataBase
    {
        public IEnumerable<UserModel> Users()
        {
            return new List<UserModel>()
            {
                new UserModel{Id = 1, Name = "James Callaghan", PIN = "2019", BVN = "012209"},
                new UserModel{Id = 2, Name = "Jane Fonda", PIN = "2020", BVN = "012210" },
                new UserModel{Id = 3, Name = "Bill Graham", PIN = "2021", BVN="012211"},
                new UserModel{Id = 4, Name = "Alistair Crowley", PIN = "1908", BVN= "012212"},
                new UserModel{Id = 5, Name = "Pete Edochie", PIN = "2012", BVN = "012213"}
            };
        }
    }
}
