﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;


namespace CustomKeyboard
{
    class Program
    {
        private static DB DB = new DB();
        static void Main(string[] args)
        {
            string strings = null;
            Console.WriteLine("write a character: ");
            var input = Console.ReadLine();
            
            foreach (char chara in input)
            {
                var dbChar = from character in DB.Characters()
                             select character;

                foreach (var character in dbChar)
                {

                    if (chara == character.character)
                    {
                        strings += character.number;
                        
                    }
                    
                    
                }
                
            }
            Console.WriteLine(strings);
            if (strings == null || strings.Length < input.Length)
            {
                Console.WriteLine("Invalid input");
            }


        }
    }

    public class DB
    {
        public IEnumerable<InputModel> Characters()
        {
            return new List<InputModel>()
            {
                new InputModel{number = "1", character = ')'},
                new InputModel{number = "2", character = '(' },
                new InputModel{number = "3", character = '*' },
                new InputModel{number = "4", character = '&' },
                new InputModel{number = "5", character = '^' },
                new InputModel{number = "6", character = '%' },
                new InputModel{number = "7", character = '$' },
                new InputModel{number = "8", character = '#' },
                new InputModel{number = "9", character = '@' },
                new InputModel{number = "0", character = '!' }
            };
        }
    }
    public class InputModel : EventArgs
    {
        public string number { get; set; }
        public char character { get; set; }
    } 
}